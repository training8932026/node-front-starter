import logo from "./logo.svg";
import "./App.css";
import { useEffect, useState } from "react";

function App() {
  const [data, setData] = useState("");
  useEffect(() => {
    fetch(`${process.env.REACT_APP_BACK_URL}/fetch`)
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        setData(data.message);
      })
      .catch((error) => {
        setData("Error");
      });
  }, []);

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>Var env : {process.env.REACT_APP_VAR_ENV}</p>
        <p>Fetch data : {data}</p>
      </header>
    </div>
  );
}

export default App;
